﻿using UnityEngine;
using System.Collections;
using System;

public class Stopwatch : MonoBehaviour {

	public event EventHandler OnChange;

	public float changeTime;
    public bool refreshKeys;
    GameObject[] players;
    float time;

    // Use this for initialization
    void Start () {

        players = GameObject.FindGameObjectsWithTag("Player");
        time = Time.time;

    }

    // Update is called once per frame
    void Update () {

        if (refreshKeys == true && Time.time - time >= changeTime)
        {
            // Refresh the Keybinds for Each Player
            foreach (GameObject player in players)
                player.GetComponent<PlayerControls>().ReturnKeys();
            foreach (GameObject player in players)
                player.GetComponent<PlayerControls>().GetKeys();

            // Reset the Time
            time = Time.time;

			if(OnChange != null)
			{
				OnChange(gameObject, new EventArgs());
			}
        }
    }
}
