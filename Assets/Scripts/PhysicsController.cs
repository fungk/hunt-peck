﻿using UnityEngine;
using System.Collections;

public class PhysicsController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player1Main"), LayerMask.NameToLayer("Player2Main"));
		Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player1Main"), LayerMask.NameToLayer("Player1Attack"));
		Physics2D.IgnoreLayerCollision (LayerMask.NameToLayer("Player2Main"), LayerMask.NameToLayer("Player2Attack"));
		Physics2D.IgnoreLayerCollision (LayerMask.NameToLayer ("Player2Attack"), LayerMask.NameToLayer("Player1Attack"));
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
