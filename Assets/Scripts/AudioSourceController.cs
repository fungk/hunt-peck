﻿using UnityEngine;
using System.Collections;

public class AudioSourceController : MonoBehaviour {

	public bool fadeMusic = false;

	AudioSource music;

	AudioSource source;

	bool initialized = false;

	// Use this for initialization
	void Awake () {
		music = GameObject.FindWithTag("Music").GetComponent<AudioSource>();

		source = GetComponent<AudioSource>();
		source.Stop ();
	}
	
	// Update is called once per frame
	void Update () {
		if(initialized && !source.isPlaying)
		{
			if(fadeMusic)
			{
				music.mute = false;
			}
			Destroy (gameObject);
		}
	}

	public void Initialize(AudioClip clip, bool fade = false)
	{
		fadeMusic = fade;
		source.clip = clip;
		source.Play ();
		initialized = true;

		if(fadeMusic)
		{
			music.mute = true;
		}
	}
}
