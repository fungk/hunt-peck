﻿using UnityEngine;
using System.Collections;

public class PunchDetector : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnTriggerEnter2D(Collider2D other)
	{
		if(other.gameObject.tag == "Player")
		{
			other.gameObject.GetComponent<PlayerControls>().Hit(Mathf.Sign (-transform.parent.localScale.x));
		}
	}
}
